﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DealerStat.Startup))]
namespace DealerStat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
