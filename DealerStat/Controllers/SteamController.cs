﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DealerStat.Data.Infrastructure;
using DealerStat.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DealerStat.Controllers
{
    public class SteamController : Controller
    {
        public string GetSteamId()
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            if (User.Identity.Name != "")
            {
                string url = currentUser.Logins.First().ProviderKey;
                ViewBag.steamid = url.Split('/')[5]; //here we going to split the providerkey so we get the right part
            }
            else
            {
                ViewBag.steamid = "";
            }

            return ViewBag.steamid;
        }

        [HttpGet]
        public ContentResult GetProfile()
        {
            string url = $"http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=10154DA98CB6FE42D2CBE20D59F0A1DC&steamids={this.GetSteamId()}";
            string result;

            using (var client = new WebClient())
            {
                result = client.DownloadString(url);
            }
            return Content(result, "application/json");
        }
    }
}