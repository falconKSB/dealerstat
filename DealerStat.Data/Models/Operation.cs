﻿namespace DealerStat.Data.Models
{
    public class Operation
    {
        public int Id { get; set; }

        public OperationType OperationType { get; set; }

        public int SenderId { get; set; }

        public int RecieverId { get; set; }
    }
}
