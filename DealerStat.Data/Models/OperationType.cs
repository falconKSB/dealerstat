﻿namespace DealerStat.Data.Models
{
    public enum OperationType
    {
        Exchange = 0,
        Buy = 1,
        Sell = 2
    }
}
