﻿namespace DealerStat.Data.Models
{
    public class Feedback
    {
        public int Id { get; set; }

        public int OperationId { get; set; }

        public int ScorePublic { get; set; }

        public int ScoreAnonim { get; set; }
    }
}
