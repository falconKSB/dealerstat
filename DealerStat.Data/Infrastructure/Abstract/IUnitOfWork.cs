﻿using System;
using System.Collections.Generic;
using System.Text;
using DealerStat.Data.Models;

namespace DealerStat.Data.Infrastructure.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
       IRepository<User> Users { get; }

       IRepository<Feedback> Feedbacks { get; }

       IRepository<Operation> Operations { get; }

        void Commit();
    }
}
