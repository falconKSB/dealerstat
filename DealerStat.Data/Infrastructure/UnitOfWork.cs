﻿using System.Data.Entity;
using DealerStat.Data.Infrastructure.Abstract;
using DealerStat.Data.Models;

namespace DealerStat.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;

        private IRepository<Feedback> _feedbacks { get; set; }
        private IRepository<User> _users { get; set; }
        private IRepository<Operation> _operations { get; set; }

        public IRepository<User> Users => _users ?? (_users = new Repository<User>(_dbContext));
        public IRepository<Feedback> Feedbacks => _feedbacks ?? (_feedbacks = new Repository<Feedback>(_dbContext));
        public IRepository<Operation> Operations => _operations ?? (_operations = new Repository<Operation>(_dbContext));

        public UnitOfWork()
        {
            _dbContext = new DealerDbContext();
            _dbContext.Configuration.LazyLoadingEnabled = true;
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

    }
}
