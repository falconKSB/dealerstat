﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DealerStat.Data.Models;

namespace DealerStat.Data.Infrastructure
{
    public class DealerDbContext : DbContext
    {
        public DbSet<Feedback> Feedbacks { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Operation> Operations { get; set; }

        public DealerDbContext() : base("DealerContext")
        {
            Database.SetInitializer<DealerDbContext>(new CreateDatabaseIfNotExists<DealerDbContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
